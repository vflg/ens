\def\inc{inc7-sig}

\newcommand {\ctrl} [2] {\framebox{#2{}CTRL}\framebox{#2{}#1}}

\titreA {Gestion des signaux}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Introduction}

\begin {frame} {Introduction}
    Définition
    \begin {quote}
	Un signal est un événement notifié par le noyau à un processus
    \end {quote}

    \vspace* {-2mm}
    
    Exemples :
    \begin {itemize}
	\item événements matériels
	    \begin {itemize}
		\item déconnexion (\code {SIGHUP}),
		\item appui sur \ctrl{C}{\fD}
		    (\code {SIGINT})
	    \end {itemize}
	\item événements suite à une action du programme
	    \begin {itemize}
		\item erreur d'adressage mémoire (\code {SIGSEGV})
		\item instruction illégale (\code {SIGILL}),
		\item alarme de processus (\code {SIGALRM}),
		\item écriture dans un tube sans lecteur (\code {SIGPIPE}), etc
	    \end {itemize}
	\item événements sans sémantique associée pour le noyau
	    \begin {itemize}
		\item signaux « utilisateur » (\code {SIGUSR1} et
		    \code {SIGUSR2})
		\item signal de terminaison (\code {SIGTERM})
		\item signal de terminaison absolu (\code {SIGKILL})
	    \end {itemize}

    \end {itemize}

    Les signaux sont représentés par des entiers \implique \code {SIG*}
\end {frame}

\begin {frame} {Introduction -- Action par défaut}
    Notification au processus \implique action par défaut du processus

    \begin {itemize}
	\item terminer le processus
	    \begin {itemize}
		\item exemple : \ctrl{C}{\fD}
		\item exemple : \code{SIGSEGV} \implique erreur
		    d'adressage mémoire
		\item certains signaux provoquent la génération d'un
		    fichier \code {core}
		    \begin {itemize}
			\item pour l'analyse de la mémoire à postériori
			\item exemple : \code {\$ gdb a.out core}
			\item peut nécessiter : \code {\$ ulimit
			    -c unlimited}

		    \end {itemize}
	    \end {itemize}
	\item ignorer le signal
	    \begin {itemize}
		\item exemple : terminaison d'un fils
		    \implique \code{SIGCHLD}
	    \end {itemize}
	\item suspendre l'exécution du processus
	    \begin {itemize}
		\item exemple : \ctrl{Z}{\fD}
		    \implique \code{SIGTSTP} (terminal stop)
		\item ou envoi de \code{SIGSTOP}
	    \end {itemize}
	\item reprendre l'exécution du processus
	    \begin {itemize}
		\item Job control : \code{fg}/\code{bg} en shell \implique
		    \code {SIGCONT}

	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Introduction -- Action par défaut}
    Quelques signaux et leurs actions par défaut :
    \ctableau {\fD} {|l|l|c|l|} {
	\textbf {Signal}
	    & \textbf {Action}
	    & \textbf {core}
	    & \textbf {Signification}
	    \\ \hline
	SIGALRM  & Terminer  &     & Alarme de processus \\
	SIGCHLD  & Ignorer   &     & Terminaison d'un fils \\
	SIGCONT  & Reprendre &     & \code{fg}/\code{bg} \\
	SIGFPE   & Terminer  & oui & Expression invalide (ex: division par 0) \\
	SIGHUP   & Terminer  &     & Déconnexion \\
	SIGINT   & Terminer  &     & \ctrl{C}{\fE} \\
	SIGKILL  & Terminer  &     & Arme atomique... \\
	SIGPIPE  & Terminer  &     & Écriture dans un tube sans lecteur \\
	SIGQUIT  & Terminer  & oui & \ctrl{\textbackslash}{\fE} \\
	SIGSEGV  & Terminer  & oui & Accès à une case mémoire invalide \\
	SIGSTOP  & Suspendre &     & Suspendre le processus \\
	SIGTERM  & Terminer  &     & Demande de terminaison du processus \\
	SIGTSTP  & Suspendre &     & \ctrl{Z}{\fE} \\
	SIGUSR1  & Terminer  &     & Signal sans définition système 1 \\
	SIGUSR2  & Terminer  &     & Signal sans définition système 2 \\
	SIGWINCH & Ignorer   &     & Changement de taille de fenêtre \\
    }
\end {frame}

\begin {frame} {Introduction -- Changement d'action}

    L'action par défaut n'est pas toujours souhaitable \\
    \implique une action spécifique peut être associée à chaque signal

    \begin {itemize}
	\item \code{SIG\_IGN} : ignorer le signal
	\item \code{SIG\_DFL} : l'action par défaut
	\item exécuter une fonction définie préalablement
	    \begin {itemize}
		\item la fonction interrompt l'exécution du programme
		\item lorsque la fonction se termine, le programme reprend
		    où il avait été interrompu
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Exemples d'utilisation des signaux}

    \begin {itemize}
	\item sauvegarder le calcul en cours en cas d'interruption
	    \begin {itemize}
		\item appui sur \ctrl{C}{\fD}
		    \implique \code {SIGINT}
		\item appeler la fonction programmée pour \code {SIGINT}
	    \end {itemize}

	\item interrompre une action sans sortir du programme
	    \begin {itemize}
		\item exemple : \ctrl{C}{\fD} avec \code{vi}
	    \end {itemize}

	\item terminer le programme « proprement »
	    \begin {itemize}
		\item l'utilisateur envoie le signal \code {SIGTERM}
		\item appeler la fonction programmée pour \code {SIGTERM}
		    \begin {itemize}
			\item sauvegarder les données en mémoire,
			    supprimer les fichiers temporaires, etc.
		    \end {itemize}
	    \end {itemize}

	\item continuer le programme même après une déconnexion
	    \begin {itemize}
		\item déconnexion \implique \code {SIGHUP}
		\item ignorer le signal
	    \end {itemize}

	\item planifier une action à exécuter dans 3 minutes
	    \begin {itemize}
		\item programmer une alarme \implique \code {SIGALRM}
		\item appeler la fonction programmée pour \code {SIGALRM}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {Cas particuliers}
    Deux signaux particuliers : impossible de modifier l'action

    \begin {itemize}
	\item \code {SIGKILL}
	    \begin {itemize}
		\item Avec les signaux, il est possible d'exécuter une fonction
		    au lieu de terminer le processus par défaut

		\item S'il est possible de programmer une fonction pour chacun
		    des signaux, on peut avoir des processus «~immortels~»

		\item D'où le signal \code {SIGKILL} :
		    \begin {itemize}
			\item action = action par défaut
			    \implique terminer le processus
			\item impossible de modifier cette action
			\item il reste toujours un moyen de terminer un processus !
			\item ne pas envoyer \code{SIGKILL} (= 9)
			    directement à un processus \\
			    \implique le processus ne peut pas se terminer
				«~proprement~» \\
			    \implique faire d'abord des tirs de sommation
				(ex : \code{SIGHUP}, \code{SIGTERM})

		    \end {itemize}

	    \end {itemize}

	\item \code {SIGSTOP}
	    \begin {itemize}
		\item Analogue à \code{SIGKILL} : 
		    suspension impérative de processus
		\item Ne pas confondre avec \code {SIGTSTP}
		    (envoyé suite à \ctrl{Z}{\fD})

	    \end {itemize}

    \end {itemize}
\end {frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API Unix v7
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {API Unix v7}

\begin {frame} {API Unix v7}
    Ensemble de primitives pour gérer les signaux :

    \vspace* {-3mm}

    \prototype {
	\code {void (*signal (int sig, void (*fct) (int sig))) (int sig)} \\
	\code {int kill (pid\_t pid, int sig)} \\
	\code {int pause (void)} \\
	\code {unsigned int alarm (unsigned int nsec)}
    }

    \vspace* {-2mm}

    \begin {itemize}
	\item primitives « originelles » (Unix v7, 1977)
	    \begin {itemize}
		\item en réalité plus anciennes
		\item mais Unix v7 très largement diffusé

	    \end {itemize}
	\item primitive \code {signal} obsolète
	    \begin {itemize}
		\item supplantée par \code {sigaction}, voir API POSIX
		\item mais \code {signal} simple, toujours utilisée
		\item et bonne introduction pédagogique
		\item et malgré tout, toujours normalisée par POSIX
	    \end {itemize}
	\item les autres primitives sont toujours d'actualité
    \end {itemize}
\end {frame}

\begin {frame} {API Unix v7 -- Primitive signal}
    \begin {minipage} [c] {.50\linewidth}
	\hspace* {-5mm}
	\includegraphics [width=1.1\linewidth] {\inc/derout}
    \end {minipage}
    \hfill
    \begin {minipage} [c] {.49\linewidth}
	\fC
	\begin {itemize}
	    \item \code {signal} définit l'action à réaliser
		lorsque le signal \textbf {arrivera}
	    \item prototype \code {fct} fixe
		\begin {itemize}
		    \fD
		    \item numéro de signal passé en
			argument
		    \item pas d'autre argument possible
		\end {itemize}
	\end {itemize}
    \end {minipage}

    \vspace* {5mm}

    Autres valeurs possibles pour la fonction de \code {signal} :

    \vspace* {-3mm}

    \ctableau {\fC} {|l|p{.8\linewidth}|} {
	\code {SIG\_IGN} & ignorer le signal \\
	\code {SIG\_DFL} & remettre l'action par défaut (plupart
	    des signaux : terminer le processus) \\
    }
\end {frame}

\begin {frame} {API Unix v7 -- Primitive signal}
    \prototype {
	\code {void (*signal (int sig, void (*fct) (int sig))) (int sig)}
    }

    \begin {itemize}
	\item quel beau prototype de fonction C...
	\item \code {signal} prend en argument : :
	    \begin {itemize}
		\item un numéro de signal pour lequel
		    l'action doit être définie
		\item l'adresse d'une fonction (dans le programme)
		    \begin {itemize}
			\item prenant en argument un entier
			    (le numéro du signal reçu)
			\item et ne renvoyant rien
		    \end {itemize}
		    ou bien :
		    \begin {itemize}
			\item \code {SIG\_DFL} : adresse == 0 
			\item \code {SIG\_IGN} : adresse == 1
		    \end {itemize}
	    \end {itemize}
	\item \code {signal} renvoie l'adresse de l'ancienne fonction
	    \begin {itemize}
		\item ou bien \code {SIG\_ERR} (adresse == -1) en cas d'erreur
		    \begin {itemize}
			\item par exemple si \code {sig} == \code {SIGKILL}
		    \end {itemize}
	    \end {itemize}
	\item attention : \code {signal (SIGHUP, fct (5))} passe en
	    argument le \textbf {résultat} de l'appel de la fonction
	    \code {fct}, et non son adresse

    \end {itemize}
\end {frame}

\begin {frame} {API Unix v7 -- Primitive signal}
    \begin {casseroleblock} {Analogie du radio-réveil}
    \begin {itemize}
	\item \code{signal} \textbf {n'attend pas} l'arrivée d'un signal
	    \begin {itemize}
		\item \code{signal} spécifie l'action à effectuer quand
		    le signal \textbf {arrivera}
	    \end {itemize}
	\item comme un radio-réveil ou un smartphone :
	    \begin {itemize}
		\item on \textbf {règle} la radio à écouter lorsque
		    le réveil \textbf {sonnera}
		\item on \textbf {indique} la chanson à écouter lorsque
		    le réveil \textbf {sonnera}
	    \end {itemize}
    \end {itemize}
    \end {casseroleblock}
\end {frame}

\begin {frame} {API Unix v7 -- Fonction appelée}
    La fonction appelée doit avoir le prototype suivant :

    \lstinputlisting [basicstyle=\fD\lstmonstyle] {\inc/proto.c}

    Notes :
    \begin {itemize}
	\item pas de possibilité de changer le type de retour
	\item pas de possibilité de changer les arguments

	\item avec les options \code {-Wall -Werror} du
	    compilateur \code {gcc}, il faut éviter de générer une
	    erreur si l'argument n'est pas utilisé :

	    \lstinputlisting [basicstyle=\fD\lstmonstyle] {\inc/unused.c}
    \end {itemize}

\end {frame}

\begin {frame} {API Unix v7 -- Fonction appelée}
    Attention à la fonction appelée lors de la réception d'un signal :

    \begin {itemize}
	\item L'appel de la fonction interrompt le programme en
	    cours
	\item Le programme pouvait faire des choses complexes

	    \begin {itemize}
		\item exemple~:

		    \lstinputlisting [basicstyle=\fE\lstmonstyle, numbers=left] {\inc/compteur.c}
	    \end {itemize}

	\item Problème :
	    \begin {itemize}
		\item si le programme est interrompu entre les lignes 11 et 12
		\item et si la fonction utilise la variable \code {c}
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {API Unix v7 -- Fonction appelée}
    Recommandations pour la fonction :

    \begin {itemize}
	\item Limiter la fonction à la modification d'une variable
	\item Tester la variable dans le programme principal
	\item Utiliser une variable « \code {volatile sig\_atomic\_t} »
	    \begin {itemize}
		\item qualificateur \code {volatile} : empêcher
		    certaines optimisations intempestives
		\item type \code {sig\_atomic\_t} : variable modifiée
		    en une seule opération
		    \\
		    \implique voir semestre prochain
	    \end {itemize}
    \end {itemize}

    \vspace* {3mm}

    Autre action dans la fonction \implique fonctionnement
    non garanti
\end {frame}

\begin {frame} {API Unix v7 -- Fonction appelée}
    Exemple :

    \lstinputlisting [basicstyle=\fD\lstmonstyle, firstline=4] {\inc/volatile.c}

\end {frame}

\begin {frame} {API Unix v7 -- Signaux et processus}
    Actions associées aux signaux :
    \begin {itemize}
	\item ce sont des attributs du processus
	\item héritées avec \code {fork}
	\item réinitialisées avec \code {exec}
    \end {itemize}
\end {frame}

\begin {frame} {API Unix v7}
    Autres primitives associées aux signaux :

    \begin {itemize}
	\item \code {int kill (pid\_t pid, int sig)}

	    \begin {itemize}
		\item envoie un signal à un processus
	    \end {itemize}

	\item \code {int pause (void)}

	    \begin {itemize}
		\item suspend l'exécution du programme en attendant
		    l'arrivée d'un signal \implique attente \textit{passive}

		\item si le signal est ignoré, \code {pause} ne
		    termine pas

		\item \code {pause} renvoie toujours -1 \implique
		    primitive interrompue par un signal

	    \end {itemize}

	\item \code {unsigned int alarm (unsigned int nsec)}

	    \begin {itemize}
		\item programme l'envoi de \code {SIGALRM}
		    au processus courant
		\item dans l'analogie du radio-réveil, \code{signal}
		    règle la radio, et \code{alarm} règle l'heure
		    de réveil

	    \end {itemize}

    \end {itemize}
\end {frame}

\begin {frame} {API Unix v7}
  
    \begin {casseroleblock} {Recommandations / rappels}
    \begin {itemize}
	\item \code {signal} \textbf {n'attend pas} l'arrivée d'un signal
	    \begin {itemize}
		\item ne fait que modifier la fonction
		\item ne pas confondre avec \code {pause}
	    \end {itemize}
	\item appeler \code {signal} avec l'\textbf {adresse} d'une fonction
	    \begin {itemize}
		\item ... et pas son résultat
		\item \code {signal (SIGINT, f)} et non
		    \code {signal (SIGINT, f(5))}
	    \end {itemize}
	\item en faire \textbf {le moins possible} dans la fonction
	    \begin {itemize}
		\item évite les problèmes de concurrence
	    \end {itemize}
    \end {itemize}
    \end {casseroleblock}
\end {frame}

\begin {frame} {API Unix v7 -- Attention piège !}
    Exemple : je veux attendre la réception d'un signal :

    \lstinputlisting [basicstyle=\fE\lstmonstyle, lastline=16, numbers=left] {\inc/sigsuspend.c}

    \medskip

    Si le signal arrive entre les lignes 13 et 14
    \implique attente éternelle

    \bigskip

    Pas possibile de gérer cela correctement avec l'API v7 !

\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Analogie avec les interruptions matérielles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Analogie avec les interruptions matérielles}

\begin {frame} {Analogie avec les interruptions matérielles}
    Mécanisme matériel :

    \begin {minipage} [c] {.40\linewidth}
	\includegraphics [width=\linewidth] {\inc/bus}
    \end {minipage}
    \begin {minipage} [c] {.59\linewidth}
	\begin {itemize}
	    \fC
	    \item lorsqu'un contrôleur a terminé une requête,
		il active la ligne d'interruption du bus de contrôle

	    \item lorsque le processeur termine l'exécution de
		l'instruction courante, il consulte la ligne d'interruption

	    \item si elle est dans l'état « actif », le processeur
		interrompt le programme en cours

	    \item le contrôleur reste « interruptif » jusqu'à ce qu'il
		soit interrogé par le processeur
	\end {itemize}
    \end {minipage}

    \vspace* {3mm}

    Trois registres du processeur impliqués :
    \ctableau {\fC} {|l|l|} {
	PC & Program Counter (compteur ordinal) \\
	SP & Stack Pointer (pointeur de pile) \\
	SR & Status Register (registre d'état) \\
    }

\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    Actions du processeur suite à une interruption~:
    \begin {enumerate}
	\item lorsque l'interruption se produit, PC pointe dans le code du
	    processus, SP dans la pile du processus et SR indique qu'on
	    est en mode «~non privilégié~» (par exemple)

	\item le processeur sauvegarde ces registres

	\item le processeur modifie ensuite ces registres :
	    \begin {itemize}
		\item SR :
		    \begin {itemize}
			\item passage en mode « privilégié »
			\item blocage (masquage) des interruptions
		    \end {itemize}

		\item PC : initialisé à partir du vecteur d'interruption

		\item SP : pointe sur la pile noyau
	    \end {itemize}

    \end {enumerate}

    \vspace* {2mm}

    \implique tout ceci est effectué par le matériel
\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    Vecteur d'interruptions :

    \begin {itemize}
	\item tableau d'adresses de fonctions internes au noyau
	\item placé à une adresse fixée pour le processeur
	\item indexé par le numéro de l'interruption
	    \begin {itemize}
		\item exemple : interruptions clavier,
		    interruptions disque, etc
	    \end {itemize}
	\item initialisé par le noyau au démarrage du système
    \end {itemize}

\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    Masquage des interruptions :

    \begin {itemize}
	\item empêche le processeur de consulter les interruptions
	\item mécanisme sélectif (selon le matériel)
	    \begin {itemize}
		\item exemple : masquer ce qui est moins
		    prioritaire que l'interruption courante

		\item masquage implicite de l'interruption courante

	    \end {itemize}

	\item masquage \implique contrôleur reste « interruptif »
	\item intérêt : empêcher le noyau de modifier une structure
	    de donnée altérée par le traitement d'une interruption
    \end {itemize}
\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    \begin {center}
	\includegraphics [width=\textwidth] {\inc/ps-except}
    \end {center}
\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    Une fois le contexte (PC, SP, SR) initialisé, le processeur exécute
    le code du noyau~:

    \begin {enumerate}
	\item (en assembleur) sauvegarde du reste du contexte CPU
	    (registres généraux, etc.)

	\item (en assembleur) mise en place d'un contexte de pile
	    pour un appel de procédure en langage de haut niveau (ex: C)

	\item (en assembleur) branchement à une adresse

	\item (en C) vérification de la raison de l'interruption

	    \begin {itemize}
		\item interrogation des contrôleurs de périphériques
		    pour identifier l'origine de l'interruption
	    \end {itemize}

	\item (en C) action correspondant à l'interruption

    \end {enumerate}
\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    Au retour~:

    \begin {itemize}
	\item actions logicielles symétriques en fin d'exception (en
	    C puis en assembleur)

	\item actions (en matériel) symétriques à la prise en compte
	    de l'exception : instruction spéciale (IRET pour x86,
	    RTE pour 68000)

    \end {itemize}
\end {frame}

\begin {frame} {Analogie avec les interruptions matérielles}
    Bilan :

    \ctableau {\fC} {|p{.20\linewidth}|p{.35\linewidth}|p{.35\linewidth}|} {
	~
	    & \multicolumn {1} {c|} {\textbf {Interruptions}}
	    & \multicolumn {1} {c|} {\textbf {Signaux}} \\
	Niveau & Matériel & Logiciel \\
	Émetteur & Périphérique & Noyau \\
	Destinataire & Processeur (noyau) & Processus \\
	Masquage & Oui & Non \\
    }
\end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API POSIX
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {API POSIX}

\begin {frame} {API POSIX}
    Problèmes avec \code {signal} (API v7) :

    \begin {itemize}
	\item pas de possibilité de masquage des signaux
	    \begin {itemize}
		\item ignorer un signal \implique signal perdu
	    \end {itemize}
	\item signal courant pas auto-masqué
	    \begin {itemize}
		\item dépendant de l'implémentation
		\item fonction associée au signal interrompue par
		    elle-même...
	    \end {itemize}
	\item action peut-être réinitialisée à l'action par défaut
	    \begin {itemize}
		\item dépendant de l'implémentation
		\item deux appuis successifs sur \ctrl{C}{\fD}
		    \implique patatras !
	    \end {itemize}
    \end {itemize}

    \vspace* {3mm}

    Gestion des signaux non fiable avec \code {signal} !
\end {frame}

\begin {frame} {API POSIX -- Primitive sigaction}
    POSIX : amélioration des signaux

    \prototype {
	\code {int sigaction (int sig, const struct sigaction *new,}
	\\
	\hspace* {.4\linewidth} {\code {struct sigaction *old)}}
    }

    \begin {itemize}
	\item \code {sigaction} remplace \code {signal}
	\item action décrite par une \code {struct sigaction} :
	    \ctableau {\fC} {|l|l|} {
		\code {sa\_handler}
		    & adresse de la fonction (ou \code {SIG\_IGN} ou
			\code {SIG\_DFL})
		    \\
		\code {sa\_mask}
		    & masque pendant l'exécution de la fonction
		    \\
		\code {sa\_flags}
		    & comportements particuliers
		    \\
	    }

	    \vspace* {1mm}

	\item masquage implicite du signal reçu pendant l'exécution
	    de la fonction
	\item pas de modification de l'action associée au signal
	\item permet de récupérer (ou pas) l'ancienne action
    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigaction}
	    \ctableau {\fC} {|l|l|} {
		\code {sa\_handler}
		    & adresse de la fonction (ou \code {SIG\_IGN} ou
			\code {SIG\_DFL})
		    \\
		\code {sa\_mask}
		    & masque pendant l'exécution de la fonction
		    \\
		\code {sa\_flags}
		    & comportements particuliers
		    \\
	    }

    \begin {itemize}
	\item \code {sa\_handler} : même type de fonction que pour
	    \code {signal}

	\item \code {sa\_mask} : signaux supplémentaires à masquer
	    pendant l'exécution de la fonction appelée

	    \begin {itemize}
		\item type \code {sigset\_t} = champ de bits
		    \begin {center}
			\includegraphics [width=.5\textwidth] {\inc/sigset}
		    \end {center}

		\item signal $i \in$ ensemble $\Leftrightarrow$ bit $i$ à 1
		\item manipulation avec des fonctions de bibliothèque
		    \ctableau {\fD} {|l|l|} {
			\code {sigemptyset}
			    & vide l'ensemble \\
			\code {sigfillset}
			    & remplit l'ensemble
			    \\
			\code {sigaddset}
			    & ajout un signal à l'ensemble
			    \\
			\code {sigdelset}
			    & retire un signal de l'ensemble
			    \\
			\code {sigismember}
			    & teste si un signal fait partie de l'ensemble
			    \\
		    }
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigaction}
    Exemple :

    \lstinputlisting [basicstyle=\fC\lstmonstyle] {\inc/sigaction.c}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigprocmask}
    \prototype {
	\code {int sigprocmask (int comment, sigset\_t *new, sigset\_t *old)}
    }

    \begin {itemize}
	\item \code {sigprocmask} : masque ou démasque des signaux
	\item pendant le masquage, le signal n'est pas perdu
	    \begin {itemize}
		\item le signal sera traité lors du démasquage
		\item attention : un seul bit pour la réception d'un signal
		\item \implique signal envoyé 2 fois : on
		    ne le traitera qu'une fois
	    \end {itemize}
	\item le masque est spécifié par l'ensemble \code {new}
	\item valeurs possibles pour \code {comment} :
	    \ctableau {\fD} {|l|l|} {
		\code {SIG\_BLOCK}
		    & signaux $\in$ \code {new} ajoutés au masque courant
		    \\
		\code {SIG\_UNBLOCK}
		    & signaux $\in$ \code {new} retirés du masque courant
		    \\
		\code {SIG\_SETMASK}
		    & masque courant $\leftarrow$ \code {new}
		    \\
	    }
    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigprocmask}
    Pourquoi/quand utiliser \code{sigprocmask} ?

    \begin {itemize}
	\item Exemple déjà vu : le programme principal appelle la
	    fonction \code{incrementer}

	    \lstinputlisting [basicstyle=\fE\lstmonstyle, numbers=left] {\inc/compteur.c}

	\item Problème de concurrence si la fonction associée à un
	    signal utilise la variable \code{c}

	\item Il faut empêcher le signal d'être pris en compte lorsque
	    \code{incrementer} s'exécute
	    \implique \textit{masquer} le signal

    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigprocmask}

    \lstinputlisting [basicstyle=\fD\lstmonstyle] {\inc/sigprocmask.c}

\end {frame}

\begin {frame} {API POSIX -- Primitive sigpending}
    \prototype {
	\code {int sigpending (sigset\_t *ensemble)}
    }
    \begin {itemize}
	\item \code {sigpending} : retourne l'ensemble des
	    signaux en attente

	\item signaux en attente \implique ils sont masqués
	    \begin {itemize}
		\item les signaux sont reçus
		\item mais ils sont masqués
		\item donc ils sont en attente de traitement
	    \end {itemize}

	\item rappel : signal $i$ reçu $\Leftrightarrow$ bit $i$ à 1
	    \begin {itemize}
		\item un signal n'est mémorisé qu'une seule fois
		\item si un signal $i$ est reçu $n$ fois ($n > 1$),
		    on ne garde qu'un bit
	    \end {itemize}
    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigsuspend}
    \prototype {
	\code {int sigsuspend (const sigset\_t *masque)}
    }
    \begin {itemize}
	\item \code {sigsuspend} : généralisation de \code {pause}
	\item attend l'arrivée d'un ou plusieurs signaux
	\item masque ou démasque temporairement (pendant
	    \code {sigsuspend}) les signaux non désirés à l'aide de
	    \code {masque}

    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigsuspend}
    Pourquoi/quand utiliser \code{sigsuspend} ?

    \begin {itemize}
	\item Exemple : prise de décision suite à un événement
	    \lstinputlisting [basicstyle=\fE\lstmonstyle, lastline=16, numbers=left] {\inc/sigsuspend.c}

	\item Si le signal arrive entre 13 et 14 \implique attente éternelle...
	    \\
	    \implique il faudrait exécuter les lignes 13 et 14 en
	    section critique
	    \\
	    tout en autorisant le signal à arriver pendant \code{pause}

    \end {itemize}
\end {frame}

\begin {frame} {API POSIX -- Primitive sigsuspend}

    \lstinputlisting [basicstyle=\fE\lstmonstyle, firstline=18, numbers=left] {\inc/sigsuspend.c}

    \begin {itemize}
	\item les lignes 12 à 15 sont exécutées en section critique
	    \begin {itemize}
		\item pendant l'attente, la section critique est levée
		\item on ne peut pas être interrompu entre 14 et 15
	    \end {itemize}
	\item ne pas abuser des sections critiques
    \end {itemize}

\end {frame}


\begin {frame} {API POSIX -- Attention piège !}

    Attention aux signaux avec \code{fork} :

    \begin {center}
	\includegraphics [width=.4\textwidth] {\inc/piege}
    \end {center}

    \begin {itemize}
	\item père envoie un signal au fils \implique le fils
	    doit se préparer

	    \begin {itemize}
		\item impossible car non déterminisme de \code{fork} :
		    le père peut être remis sur le processeur avant
		    que le fils n'ait eu le temps de démarrer
		\item seule solution : préparer la réception des signaux
		    \alert{dans le père} (avant \code{fork}) afin d'en
		    \alert{hériter} dans le fils
	    \end {itemize}

	\item problème similaire si le fils envoie un signal au père :

	    \begin {itemize}
		\item si le père se prépare après \code{fork}, le
		    fils démarrera peut-être trop rapidement !
		\item solution : le père doit se préparer \alert{avant}
		    l'appel à \code{fork}

	    \end {itemize}

    \end {itemize}
\end {frame}


\begin {frame} {API POSIX -- Bilan}

    \begin {itemize}
	\item API v7 :
	    \begin {itemize}
		\item simple
		\item insuffisante pour les cas réels
		\item pratique pour le « quick and dirty »
		\item utilisation pas à encourager
	    \end {itemize}

	    \vspace* {2mm}

	\item API POSIX :
	    \begin {itemize}
		\item adaptée au monde réel
		\item similaire aux interruptions matérielles (masquage)
		\item plus complexe, plus riche
		\item mais aussi plus robuste et plus fiable
		\item usage à privilégier
	    \end {itemize}
    \end {itemize}
\end {frame}
