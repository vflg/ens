#
# Cibles possibles :
# all: tous les chapitres individuels (chx-yyy.pdf)
# chx-yyy.pdf: un chapitre individuel particulier
# ...
# tout.pdf: document contenant tous les chapitres de la deuxième partie
# print: tous les PDF au format "3 pages par page"
#

.SUFFIXES:	.pdf .fig .svg .gnu .tex

.fig.pdf:
	fig2dev -L pdf $*.fig $*.pdf

.svg.pdf:
	inkscape --export-pdf=$*.pdf $*.svg

.gnu.pdf:
	gnuplot < $*.gnu > $*.pdf

.tex.pdf:
	pdflatex $*
	pdflatex $*

# pour la cible print
PRINTCMD = pdfjam --quiet --paper a4paper --keepinfo \
	--nup 2x3 --frame true --delta "0.2cm 0.2cm" --scale 0.95

DEPS	= courspda.sty casserole.pdf logo-uds.pdf \
	annee.tex \
	licence.tex by-nc.pdf

##############################################################################
# Introduction

SRCintro = ch1-intro.tex sl1-intro.tex

FIGintro = \
	inc1-intro/acces.pdf \
	inc1-intro/trap-lib.pdf \
	inc1-intro/boot.pdf \
	inc1-intro/histsize.pdf \
	inc1-intro/casm.pdf \
	inc1-intro/subsys.pdf \

IMGintro = \
	inc1-intro/bwk-lions.jpg \

LSTintro = \

##############################################################################
# X86

SRCx86 = ch2-x86.tex sl2-x86.tex

FIGx86 = \
	inc2-x86/asm.pdf \
	inc2-x86/c-asm.pdf \
	inc2-x86/adrmem.pdf \
	inc2-x86/mmu-i386.pdf \
	inc2-x86/reg-gen.pdf \
	inc2-x86/reg-seg.pdf \
	inc2-x86/reg-eflags.pdf \
	inc2-x86/reg-ctrl.pdf \
	inc2-x86/prot.pdf \
	inc2-x86/cpl.pdf \
	inc2-x86/tss.pdf \
	inc2-x86/irq.pdf \
	inc2-x86/io-1.pdf \
	inc2-x86/io-2.pdf \
	inc2-x86/kbd-ctrl.pdf \
	inc2-x86/bigpic-1.pdf \
	inc2-x86/bigpic-2.pdf \

IMGx86 = \

LSTx86 = \
	inc2-x86/kbd-read.s \
	inc2-x86/kbd-led.s \


##############################################################################
# L'ensemble

SRCall = \
	$(SRCintro) \
	$(SRCx86) \
	tout.tex

FIGall = \
	$(FIGintro) \
	$(FIGx86) \

IMGall = \
	$(IMGintro) \
	$(IMGx86) \

LSTall = \
	$(LSTintro) \
	$(LSTx86) \

##############################################################################
# Les cibles
##############################################################################

all:	ch1-intro.pdf \
	ch2-x86.pdf \

ch1-intro.pdf:	$(DEPS) $(FIGintro) $(IMGintro) $(LSTintro) $(SRCintro)
ch2-x86.pdf:	$(DEPS) $(FIGx86) $(IMGx86) $(LSTx86) $(SRCx86)

##############################################################################
# Stats sur les noyaux
##############################################################################

# Clone les dépôts et réalise les statistiques
# lent : ~20 min
init-kernstats:
	cd inc1-intro && sh ../kern-stats.sh init	# clone 3 repos
	cd inc1-intro && sh ../kern-stats.sh histdata	# genere kernsize-*.dat
	cd inc1-intro && sh ../kern-stats.sh casmdata > casm.dat
	cd inc1-intro && sh ../kern-stats.sh subdata > subsys.dat

# Supprime les dépôts (sans supprimer les .dat générés par init-kernstats)
clean-kernstats:
	cd inc1-intro && sh ../kern-stats.sh clean

##############################################################################
# Figures particulières
##############################################################################

inc1-intro/histsize.pdf: kern-stats.sh \
		inc1-intro/kernsize-bell.dat \
		inc1-intro/kernsize-bsd.dat \
		inc1-intro/kernsize-freebsd.dat \
		inc1-intro/kernsize-linux.dat
	cd inc1-intro && sh ../kern-stats.sh histplot > histsize.pdf

inc1-intro/casm.pdf: kern-stats.sh inc1-intro/casm.dat
	cd inc1-intro && sh ../kern-stats.sh casmplot < casm.dat > casm.pdf

inc1-intro/subsys.pdf: kern-stats.sh inc1-intro/subsys.dat
	cd inc1-intro && sh ../kern-stats.sh subplot < subsys.dat > subsys.pdf

inc2-x86/io-1.pdf: inc2-x86/io.fig
	./figlayers 49-60       < $< | fig2dev -L pdf /dev/stdin $@
inc2-x86/io-2.pdf: inc2-x86/io.fig
	./figlayers 49-60 40    < $< | fig2dev -L pdf /dev/stdin $@

inc2-x86/bigpic-1.pdf: inc2-x86/bigpic.fig
	./figlayers 40-49 50-99   < $< | fig2dev -L pdf /dev/stdin $@
inc2-x86/bigpic-2.pdf: inc2-x86/bigpic.fig
	./figlayers 30-39 50-99   < $< | fig2dev -L pdf /dev/stdin $@


tout.pdf:	$(DEPS) $(FIGall) $(LSTall) $(SRCall)

print:	print-tout.pdf

print-tout.pdf: tout.pdf
	$(PRINTCMD) -o print-tout.pdf tout.pdf

clean:
	rm -f $(FIGall)
	rm -f *.bak */*.bak *.nav *.out *.snm *.vrb *.log *.toc *.aux
	rm -f print-*.pdf ch*.pdf tout*.pdf by-nc.pdf casserole.pdf
	rm -f inc?-?-*/a.out
