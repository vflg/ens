struct machin {
  int a ; float b ; char *c ;	// exemple avec 3 arguments
} ;

int main (...) {
  struct machin m = { ... } ;	// variable m déclarée dans le thread principal
  ...
  pthread_create (..., f, &m) ;	// pas besoin de cast car 4e arg est de type void *
}

void *f (void *arg) {
  struct machin *m = arg ;	// pas besoin de cast car arg est de type void *
  ...
}
