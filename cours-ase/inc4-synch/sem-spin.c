P (S) {
    spin_lock (S.verrou1) ;
    while (S.compteur <= 0)
	;		// attente active
    spin_lock (S.verrou2) ;
    S.compteur-- ;
    spin_unlock (S.verrou2) ;
    spin_unlock (S.verrou1) ;
}

V (S) {
    spin_lock (S.verrou2) ;
    S.compteur++ ;
    spin_unlock (S.verrou2) ;
}
