condition c ;
mutex m ;
file *f ;

void *thread_traite (void *arg) {
    lock (&m) ;
    if (file_vide(f)) {
	cwait (&c, &m) ;  // \alert{$\leftarrow$ ici}
    }
    unlock (&m) ;
    traiter (extraire_file (f)) ;
}

void *thread_produit (void *arg) {
    d = lire_donnee (...) ;
    lock (&m) ;
    ajouter_file (f, d) ;
    csignal (&c) ;
    unlock (&m) ;
}
